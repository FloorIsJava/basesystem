# Default System

## Locale settings

Default keymap is `de-latin1-nodeadkeys`, `$LANG` is `en_US.UTF-8`.

Timezone default is `Europe/Berlin`.

## `blkid`

The output of `blkid` is the following for the local setup.

```
/dev/sda2: UUID="ae7f5db9-d737-4a1f-bc51-216ceeaf1132" TYPE="crypto_LUKS" PARTLABEL="Linux filesystem" PARTUUID="36303287-1d87-4de0-837a-b03e84a35c6f"
/dev/sda3: UUID="b4f88648-9537-458d-bcd7-9b4b72a21e8d" TYPE="crypto_LUKS" PARTLABEL="Linux LUKS" PARTUUID="de48d21f-aacb-464f-9a52-dd02619090da"
/dev/mapper/cryptlvm: UUID="fg6Y0m-qOm9-93KZ-8C17-qXfV-Aa0n-808Hhw" TYPE="LVM2_member"
/dev/mapper/RootGroup-root: UUID="df5c9ef9-eef6-4018-a255-fc8732641684" TYPE="ext4"
/dev/mapper/cryptboot: UUID="c8be2587-33d5-4924-a82c-15f0d6a209d2" TYPE="ext4"
/dev/sda1: PARTLABEL="BIOS boot partition" PARTUUID="abb836d7-3153-475b-9917-ab38880ee424"
```

Change disk UUIDs accordingly in the following places:

* /etc/crypttab
* /etc/default/grub
* /etc/fstab

## Partition table of `/dev/sda`

```
Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048            4095   1024.0 KiB  EF02  BIOS boot partition
   2            4096         1052671   512.0 MiB   8300  Linux filesystem
   3         1052672        67108830   31.5 GiB    8309  Linux LUKS
```

## Networking

By default, an ethernet connection providing DHCP is used. The interface is called `enp0s3`.
Create netctl profiles as necessary.

## Non-root user

The non-root user is called `lordkorea`. It is up to you to replace that name by something else in all relevant files.
