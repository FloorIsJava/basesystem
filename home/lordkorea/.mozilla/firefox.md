# Firefox Setup

## UI Customization:

The toolbar contains the following in that order:

* Back
* Forward
* Address Bar
* Reload
* Downloads

The bookmark toolbar is enabled. The density is set to compact.

## Preferences

### General

Default-browser checking is enabled.

Ctrl+Tab cycles in actual order, not in recently used order.

Disable spell check.

Disable feature and extension recommendations.

#### Fonts

* Serif: Fira Sans (14)
* Sans: Fira Sans (14)
* Mono: Fira Mono (12)

Proportional font uses serif as default.

### Home

New windows, homepage and new tabs open blank pages.

Disable all Firefox Home Content.

### Search

Default search engine is DuckDuckGo.

Search suggestions are disabled.

One-Click search engines are:

* Google (g)
* DuckDuckGo
* Wikipedia (w)

### Browser Privacy

Enhanced Tracking Protection is set to Standard.

Firefox deletes cookies and site data on close.

Login saving is disabled.

History (custom): Clear on close (everything).

Address bar suggestions are sourced from bookmarks only.

Accessibility services are blocked.

Data Collection is completely disabled, studies are opted out of.

## about:config

`media.peerconnection.enabled` is set to false to disable WebRTC.

`javascript.options.wasm` is set to false to disable WebAssembly.

`extensions.pocket.enabled` is set to false to disable Pocket.

`identity.fxaccounts.enabled` is set to false to disable Sync.

## Addons

The following addons are installed:

* uBlock Origin by Raymond Hill
* uMatrix by Raymond Hill
* HTTPS Everywhere by EFF Technologists
* Mailvelope by Mailvelope GmbH
* Certificate Pinner by Heurekus

All addons are allowed to be run in private windows.

### uBlock Origin

In the settings, tick "I am an advanced user".

Prevent CSP reports and WebRTC IP leaking.

Add the following rules:

```
* * 1p-script block
* * 3p block
* * 3p-script block
* * inline-script block
```

#### Filter Lists

Additionally to the defaults, enable the following:

* Adblock Warning Removal List
* AdGuard Base List
* AdGuard Mobile Ads
* AdGuard Tracking Prevention
* Fanboy's Enhanced Tracking List

### uMatrix

Disable automatic browser cache clearing.

Add the following rules:

```
* * * block
* * cookie block
* * css allow
* * frame block
* * image allow
* * media block
* * other block
* * script allow
* * xhr allow
```

### HTTPS Everywhere

Enable "Encrypt All Sites Eligible".
