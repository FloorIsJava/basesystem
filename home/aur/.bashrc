#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Edit path
export PATH="$HOME/.local/bin:$PATH"

source ~/.alias
export EDITOR=vim
export PS1="\[\033[38;5;10m\]\u\[\033[38;5;15m\]@\[\033[38;5;12m\]\h\[\033[38;5;15m\] \W \\$ "
