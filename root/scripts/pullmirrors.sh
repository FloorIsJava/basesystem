#!/bin/sh

curl -s "https://www.archlinux.org/mirrorlist/?country=DE&protocol=https&use_mirror_status=on" | sed -e "s/^#Server/Server/" -e "/^#/d" | rankmirrors -n 10 - > /tmp/mirrorlist

echo "Wrote mirrorlist to /tmp/mirrorlist."
echo "To complete the pull, copy the mirrorlist to /etc/pacman.d/mirrorlist."
